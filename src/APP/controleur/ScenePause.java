package APP.controleur;

import CORE.controleur.ASceneJeu;
import CORE.controleur.Const;
import CORE.controleur.IScene;
import CORE.controleur.SpriteCollection;
import processing.core.PImage;

public class ScenePause implements IScene {
    private FenetreControleur f;
    private int actif;
    private boolean entree = false;
    private ASceneJeu spCourante;
    private PImage[] pCurseur;
    private boolean sortirPause = false;
    private boolean nouveauJeu = false;
    private boolean quitterJeu = false;

    public ScenePause(FenetreControleur f, ASceneJeu ASceneJeu) {
        this.f = f;
        this.pCurseur = new PImage[]{
                SpriteCollection.get("Pause_Reprendre.png"),
                SpriteCollection.get("Pause_Nouveau.png"),
                SpriteCollection.get("Pause_Quitter.png")
        };
        actif = 0;
        spCourante = ASceneJeu;
    }

    @Override
    public IScene dessiner() {
        f.image(pCurseur[actif], Const.FENETRE_LARGEUR / 2 - pCurseur[actif].width / 2,
                Const.FENETRE_HAUTEUR / 2 - pCurseur[actif].height / 2);
        if ((entree && actif == 0) || sortirPause) {
            return spCourante;
        } else if ((entree && actif == 1) || nouveauJeu) {
            return new Scene1(f);
        } else if ((entree && actif == 2) || quitterJeu) {
            System.exit(0);
        }
        return this;
    }

    @Override
    public void keyPressed() {
        if (f.keyPressed) {
            if (f.key == processing.core.PConstants.CODED) {
                if (f.keyCode == processing.core.PConstants.UP) {
                    if (actif > 0) actif--;
                } else if (f.keyCode == processing.core.PConstants.DOWN) {
                    if (actif < 2) actif++;
                }
            } else if (f.keyCode == processing.core.PConstants.ENTER || f.keyCode == processing.core.PConstants.RETURN) {
                entree = true;
            } else if (f.key == 'p' || f.key == 'P') {
                sortirPause = true;
            } else if (f.key == 'q' || f.key == 'Q') {
                quitterJeu = true;
            } else if (f.key == 'n' || f.key == 'N') {
                nouveauJeu = true;
            }
        }
    }
}
