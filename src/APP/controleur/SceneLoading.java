package APP.controleur;

import CORE.controleur.Const;
import CORE.controleur.IScene;
import CORE.controleur.SpriteCollection;
import processing.core.PConstants;

public class SceneLoading implements IScene {

    //Taille de la barre de chargement
    private final int BARRE_HAUTEUR = 50;
    private final int BARRE_LARGEUR = 400;
    private final int BARRE_BORDURE = 10;

    private FenetreControleur f;

    public SceneLoading(final FenetreControleur f) {
        this.f = f;

        //Démarrer le thread de chargement
        new Thread(new Runnable() {
            @Override
            public void run() {
                SpriteCollection.init(f);
            }
        }).start();
    }

    @Override
    public IScene dessiner() {
        //Mettre a jour la taille de la barre
        int largeur = (int) (SpriteCollection.getProgress() * BARRE_LARGEUR);

        //dessiner la scene
        f.background(0);
        f.noFill();
        f.stroke(200);
        f.strokeWeight(2);
        f.rect(Const.FENETRE_LARGEUR / 2 - BARRE_LARGEUR / 2 - BARRE_BORDURE, Const.FENETRE_HAUTEUR / 2 - BARRE_HAUTEUR / 2 - BARRE_BORDURE, BARRE_LARGEUR + BARRE_BORDURE * 2, BARRE_HAUTEUR + BARRE_BORDURE * 2);
        f.noStroke();
        f.fill(255, 0, 0);
        f.rect(Const.FENETRE_LARGEUR / 2 - BARRE_LARGEUR / 2, Const.FENETRE_HAUTEUR / 2 - BARRE_HAUTEUR / 2, largeur, BARRE_HAUTEUR);
        f.fill(0);
        f.textSize(20);
        f.textAlign(PConstants.CENTER, PConstants.CENTER);
        f.text("CHARGEMENT EN COURS...", Const.FENETRE_LARGEUR / 2, Const.FENETRE_HAUTEUR / 2);
        return largeur >= BARRE_LARGEUR ? new SceneMenu(f) : this;
    }

    @Override
    public void keyPressed() {

    }
}
