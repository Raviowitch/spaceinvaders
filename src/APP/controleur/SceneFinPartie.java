package APP.controleur;

import CORE.controleur.Const;
import CORE.controleur.IScene;
import CORE.controleur.SpriteCollection;
import processing.core.PConstants;
import processing.core.PImage;

public class SceneFinPartie implements IScene {

    private FenetreControleur f;
    private int actif;
    private boolean entree = false;
    private PImage[] pCurseur;

    public SceneFinPartie(FenetreControleur f, boolean gagnant) {
        this.f = f;
        this.pCurseur = new PImage[]{
                SpriteCollection.get("Fin_Win_Rejouer.png"),
                SpriteCollection.get("Fin_Win_Quitter.png"),
                SpriteCollection.get("Fin_Lose_Rejouer.png"),
                SpriteCollection.get("Fin_Lose_Quitter.png")
        };
        if (gagnant) {
            actif = 0;
        } else actif = 2;
    }

    @Override
    public IScene dessiner() {
        f.image(pCurseur[actif], Const.FENETRE_LARGEUR / 2 - pCurseur[actif].width / 2,
                Const.FENETRE_HAUTEUR / 2 - pCurseur[actif].height / 2);
        if (entree && (actif == 0 || actif == 2)) {
            return new Scene1(f);
        } else if (entree && (actif == 1 || actif == 3)) {
            System.exit(0);
        }
        return this;
    }

    @Override
    public void keyPressed() {
        if (f.keyPressed) {
            if (f.key == processing.core.PConstants.CODED) {
                if (f.keyCode == PConstants.LEFT) {
                    if (actif == 1 || actif == 3) actif--;
                } else if (f.keyCode == PConstants.RIGHT) {
                    if (actif == 0 || actif == 2) actif++;
                }
            } else if (f.keyCode == processing.core.PConstants.ENTER || f.keyCode == processing.core.PConstants.RETURN) {
                entree = true;
            }
        }
    }
}
