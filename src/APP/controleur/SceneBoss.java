package APP.controleur;

import APP.GroupeVaisseaux.GroupeBoss;
import APP.modele.EnnemiC;
import APP.modele.MobileBoss;
import APP.modele.MobileHeros;
import APP.modele.MobileMur;
import APP.vue.*;
import CORE.controleur.ASceneJeu;
import CORE.controleur.Const;
import CORE.controleur.IScene;
import CORE.modele.AEnnemi;
import CORE.modele.IMobile;
import CORE.modele.Position;
import CORE.vaisseauxGraphiques.Vaisseau;
import CORE.vue.IVue;
import processing.core.PConstants;

/**
 * Scene Boss
 */
public class SceneBoss extends ASceneJeu {
    private static final int NB_VAISSEAUX = 12;
    private Vaisseau boss;
    private GroupeBoss groupeBoss;

    public SceneBoss(FenetreControleur f, MobileHeros ancienHeros) {
        super("BIEN JOUE! MAINTENANT TUES-MOI LIKE A BOSS!", f, "sceneboss.jpg");

        //prolonger la durée de l'intro
        DUREE_INTRO = 600; //10s

        //Bloquer le tir des ennemis
        AEnnemi.setNbTirsSimultanes(0);

        //créer le boss et ses copains =)
        groupeBoss = new GroupeBoss();
        int t = 360 / NB_VAISSEAUX;
        for (int i = 1; i <= NB_VAISSEAUX; i++) {
            IMobile mobile = new EnnemiC(t * (Math.PI / 180), new Position(100, 100), this);
            IVue vue = i % 2 == 0 ? new SpriteA(this, mobile) : new SpriteB(this, mobile);
            groupeBoss.ajouter(new Vaisseau(mobile, vue));
            t += 360 / NB_VAISSEAUX;
        }

        Position p = new Position(450, 200);
        IMobile bossM = new MobileBoss(p, this);
        IVue bossV = new SpriteBoss(this, bossM);
        boss = new Vaisseau(bossM, bossV);
        groupeBoss.ajouter(boss);
        this.vaisseaux.add(groupeBoss);

        //sortir le groupe de l'ecran
        groupeBoss.deplacerA(Const.FENETRE_LARGEUR / 2, -Const.FENETRE_HAUTEUR / 3);

        // Creation des murs de protection
        int xMur = Const.FENETRE_BORDURE, yMur = Const.FENETRE_HAUTEUR * 3 / 4;
        for (int k = 0; k < 5; k++) {
            for (int i = 0; i < 55; i++) {
                if ((i / 5) % 2 == 0) {
                    IMobile mobile = new MobileMur(new Position(xMur, yMur));
                    IVue vue = new SpriteMur(this, mobile);
                    vaisseaux.add(new Vaisseau(mobile, vue));
                }
                xMur += SpriteMur.LARGEUR;
            }
            xMur = Const.FENETRE_BORDURE;
            yMur += SpriteMur.HAUTEUR;
        }

        // Restaurer le heros avec le nombre de vie restante
        ancienHeros.deplacerA(Const.FENETRE_LARGEUR / 2, Const.FENETRE_HAUTEUR - 100);
        MobileHeros herosMobile = new MobileHeros(ancienHeros, this);
        IVue herosVue = new SpriteHeros(this, herosMobile);
        vaisseaux.add(new Vaisseau(herosMobile, herosVue));
    }

    /**
     * Surcharger le dessin de l'introduction
     */
    @Override
    protected void dessinerIntro() {
        if (duree_compteur <= 120)
            //le titre
            super.dessinerIntro();
        else {
            //l'animation
            f.image(fond, 0, 0);
            groupeBoss.dessiner();

            if (groupeBoss.getBas() <= Const.FENETRE_HAUTEUR * 3 / 4)
                groupeBoss.deplacer(0, 2);
            else {
                f.fill(255);
                f.textAlign(PConstants.CENTER, PConstants.CENTER);
                f.textSize(50);
                f.text("READY ???", groupeBoss.getGauche() / 2 + groupeBoss.getDroite() / 2,
                        groupeBoss.getHaut() / 2 + groupeBoss.getBas() / 2);
                f.noFill();
            }

            //fin de l'animation : activer le tir
            if (duree_compteur == DUREE_INTRO)
                AEnnemi.setNbTirsSimultanes(20);
        }
    }

    @Override
    public IScene scenePause() {
        return new ScenePause(f, this);
    }

    @Override
    public IScene sceneSuivante() {
        int[] nb = compterVaisseau();

        if (!groupeBoss.contient(boss)) //le boss est mort
            return new SceneFinPartie(f, true);
        else if (nb[0] == 0) //le heros est mort
            return new SceneFinPartie(f, false);
        else if (nb[1] == 0) //tous les ennemis sont tués
            return new SceneFinPartie(f, true);

        return this;
    }
}
