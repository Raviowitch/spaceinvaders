package APP.controleur;

import CORE.controleur.Const;
import CORE.controleur.IScene;
import processing.core.PApplet;

/**
 * Fenetre principale
 */
public class FenetreControleur extends PApplet {
    IScene sceneCourante; //stocker la scène courante

    @Override
    public void setup() {
        //Taille de la fenetre
        size(Const.FENETRE_LARGEUR, Const.FENETRE_HAUTEUR);
        frameRate(60);

        //Première scène
        sceneCourante = new SceneLoading(this);
    }

    /**
     * Dessiner la scène courante
     */
    @Override
    public void draw() {
        sceneCourante = sceneCourante.dessiner();
    }

    /**
     * Envoyer les touches capturées
     */
    @Override
    public void keyPressed() {
        sceneCourante.keyPressed();
    }
}
