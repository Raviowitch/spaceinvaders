package APP.controleur;

import APP.GroupeVaisseaux.GroupeScene3;
import APP.modele.*;
import APP.vue.*;
import CORE.controleur.ASceneJeu;
import CORE.controleur.Const;
import CORE.controleur.IScene;
import CORE.modele.AEnnemi;
import CORE.modele.IMobile;
import CORE.modele.Position;
import CORE.vaisseauxGraphiques.Vaisseau;
import CORE.vue.IVue;

import java.util.Random;

public class Scene3 extends ASceneJeu {
    private static final int NB_VAISSEAUX = 12;
    private int nbEnnemiTotal;

    private MobileHeros herosMobile;

    public Scene3(FenetreControleur f, MobileHeros ancienHeros) {
        super("Niveau 3: Ne pas gaspiller!", f, "scene3.jpg");

        Random rand = new Random();

        //Renforcer la capacité de tir des ennemis
        AEnnemi.setNbTirsSimultanes(10);

        // Creation des ennemis du groupe EnnemiC
        int t = 360 / NB_VAISSEAUX;
        for (int i = 1; i <= NB_VAISSEAUX; i++) {
            AEnnemi mobile = new EnnemiC(t * (Math.PI / 180), new Position(100, 100), this);
            IVue vue = i % 2 == 0 ? new SpriteA(this, mobile) : new SpriteB(this, mobile);

            //chacun porte un item par harsard
            IMobile itemM;
            IVue itemV;
            switch (rand.nextInt(3)) {
                case 0:
                    itemM = new MobileItem("missile", mobile.getPosition());
                    itemV = new SpriteItemMissile(this, itemM);
                    break;
                case 1:
                    itemM = new MobileItem("vie", mobile.getPosition());
                    itemV = new SpriteItemVie(this, itemM);
                    break;
                default:
                    itemM = new MobileItem("bomb", mobile.getPosition());
                    itemV = new SpriteItemBomb(this, itemM);
                    break;
            }
            mobile.ajouterItem(new Vaisseau(itemM, itemV));

            vaisseaux.add(new Vaisseau(mobile, vue));
            t += 360 / NB_VAISSEAUX;
            nbEnnemiTotal++;
        }

        // Creation des ennemis du groupe EnnemiD
        for (int i = 1; i <= NB_VAISSEAUX; i++) {
            IMobile mobile = new EnnemiD(t * (Math.PI / 180), new Position(100, 100), this);
            IVue vue = i % 2 == 0 ? new SpriteA(this, mobile) : new SpriteB(this, mobile);
            vaisseaux.add(new Vaisseau(mobile, vue));
            t += 360 / NB_VAISSEAUX;
            nbEnnemiTotal++;
        }

        // Creation des ennemis du groupe EnnemiA
        int x = 50;
        int y = 20;
        int j = 1;
        GroupeScene3 groupe = new GroupeScene3();
        for (int i = 1; i <= NB_VAISSEAUX * 3; i++) {
            AEnnemi mobile = new EnnemiA(new Position(x, y), this);
            IVue vue = i % 2 == 0 ? new SpriteA(this, mobile) : new SpriteB(this, mobile);

            //chacun porte un item par harsard
            IMobile itemM;
            IVue itemV;
            switch (rand.nextInt(3)) {
                case 0:
                    itemM = new MobileItem("missile", mobile.getPosition());
                    itemV = new SpriteItemMissile(this, itemM);
                    break;
                case 1:
                    itemM = new MobileItem("vie", mobile.getPosition());
                    itemV = new SpriteItemVie(this, itemM);
                    break;
                default:
                    itemM = new MobileItem("bomb", mobile.getPosition());
                    itemV = new SpriteItemBomb(this, itemM);
                    break;
            }
            mobile.ajouterItem(new Vaisseau(itemM, itemV));

            groupe.ajouter(new Vaisseau(mobile, vue));
            if (j == NB_VAISSEAUX) {
                y += vue.getHauteur();
                x = vue.getLargeur();
                j = 1;
            } else {
                x += vue.getLargeur();
                j++;
            }
            nbEnnemiTotal++;
        }
        vaisseaux.add(groupe);

        // Creation des murs de protection
        int xMur = Const.FENETRE_BORDURE, yMur = Const.FENETRE_HAUTEUR * 3 / 4;
        for (int k = 0; k < 5; k++) {
            for (int i = 0; i < 55; i++) {
                if ((i / 5) % 2 == 0) {
                    IMobile mobile = new MobileMur(new Position(xMur, yMur));
                    IVue vue = new SpriteMur(this, mobile);
                    vaisseaux.add(new Vaisseau(mobile, vue));
                }
                xMur += SpriteMur.LARGEUR;
            }
            xMur = Const.FENETRE_BORDURE;
            yMur += SpriteMur.HAUTEUR;
        }

        // Restaurer le heros avec le nombre de vie restante
        ancienHeros.deplacerA(Const.FENETRE_LARGEUR / 2, Const.FENETRE_HAUTEUR - 100);
        herosMobile = new MobileHeros(ancienHeros, this);
        IVue herosVue = new SpriteHeros(this, herosMobile);
        vaisseaux.add(new Vaisseau(herosMobile, herosVue));
    }

    private void afficherScore() {
        f.textSize(40);
        f.text((nbEnnemiTotal - compterVaisseau()[1]) + "/" + nbEnnemiTotal, 100, 40);
    }

    @Override
    public IScene dessiner() {
        IScene resultat = super.dessiner();
        if (resultat != this)
            return resultat;
        if (duree_compteur == DUREE_INTRO) afficherScore();
        return this;
    }

    @Override
    public IScene scenePause() {
        return new ScenePause(f, this);
    }

    @Override
    public IScene sceneSuivante() {
        int[] nb = compterVaisseau();

        if (nb[0] == 0) //l'heros est mort
            return new SceneFinPartie(f, false);
        else if (nb[1] == 0) //tous les ennemis sont tués
            return new SceneBoss(f, herosMobile);
        return this;
    }
}
