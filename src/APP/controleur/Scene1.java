package APP.controleur;

import APP.GroupeVaisseaux.GroupeScene1;
import APP.modele.EnnemiA;
import APP.modele.EnnemiSpecial;
import APP.modele.MobileHeros;
import APP.modele.MobileItem;
import APP.vue.*;
import CORE.controleur.ASceneJeu;
import CORE.controleur.Const;
import CORE.controleur.IScene;
import CORE.modele.AEnnemi;
import CORE.modele.IMobile;
import CORE.modele.Position;
import CORE.vaisseauxGraphiques.Vaisseau;
import CORE.vue.IVue;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Scene Niveau 1
 */
public class Scene1 extends ASceneJeu {
    private MobileHeros herosMobile;

    public Scene1(FenetreControleur f) {
        super("Niveau 1: Chasser Homer!", f, "universFond.jpg");

        //créer les ennemis
        GroupeScene1 groupe = new GroupeScene1();
        for (int x = 0; x < 10; x++) {

            //Initialiser le vaisseau
            AEnnemi mobile = new EnnemiA(new Position(50 + x * SpriteA.LARGEUR, 150), this);
            IVue vue = new SpriteA(this, mobile);

            //chacun porte un missile comme item
            IMobile missile = new MobileItem("missile", mobile.getPosition());
            IVue missileIcone = new SpriteItemMissile(this, missile);
            mobile.ajouterItem(new Vaisseau(missile, missileIcone));

            //Ajouter le dans le groupe
            groupe.ajouter(new Vaisseau(mobile, vue));
        }
        vaisseaux.add(groupe);

        //Créer dans 15s un vaisseau spécial qui porte la balle niveau 2
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                appelerSimpsons();
            }
        }, 15000);

        // Creation du heros avec 3 vie
        this.herosMobile = new MobileHeros(new Position(Const.FENETRE_LARGEUR / 2, Const.FENETRE_HAUTEUR - 100), this, f, 3);
        IVue herosVue = new SpriteHeros(this, herosMobile);
        vaisseaux.add(new Vaisseau(herosMobile, herosVue));
    }

    /**
     * Créer Homer Simpsons qui traverse l'écran
     */
    private void appelerSimpsons() {
        //Ennemi
        AEnnemi mobile = new EnnemiSpecial(new Position(-100, 20), this, 2);
        IVue vue = new SpriteSimpsons(this, mobile);

        //Item
        IMobile item = new MobileItem("bullet2", mobile.getPosition());
        IVue itemIcone = new SpriteItemBullet(this, item);

        //Ajouter l'item
        mobile.ajouterItem(new Vaisseau(item, itemIcone));

        //Ajouter l'ennemi
        vaisseaux.add(new Vaisseau(mobile, vue));
    }

    @Override
    public IScene scenePause() {
        return new ScenePause(f, this);
    }

    @Override
    public IScene sceneSuivante() {
        int[] nb = compterVaisseau();

        if (nb[0] == 0) //le heros est mort
            return new SceneFinPartie(f, false);
        else if (vaisseaux.size() == nb[0]) //le heros est le seul vaisseau qui reste
            return new Scene2(f, herosMobile);

        return this;
    }
}
