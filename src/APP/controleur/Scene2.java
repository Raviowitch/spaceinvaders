package APP.controleur;

import APP.GroupeVaisseaux.GroupeScene2;
import APP.modele.EnnemiB;
import APP.modele.MobileHeros;
import APP.modele.MobileItem;
import APP.vue.*;
import CORE.controleur.ASceneJeu;
import CORE.controleur.Const;
import CORE.controleur.IScene;
import CORE.modele.AEnnemi;
import CORE.modele.IMobile;
import CORE.modele.Position;
import CORE.vaisseauxGraphiques.Vaisseau;
import CORE.vue.IVue;

import java.util.Random;

/**
 * Scene Niveau 2
 */
public class Scene2 extends ASceneJeu {
    private MobileHeros herosMobile;
    private int nbEnnemiTotal; //Pour l'affichage d'un stat

    public Scene2(FenetreControleur f, MobileHeros ancienHeros) {
        super("Niveau 2: Gotta catch 'em all!", f, "space.jpg");

        //Renforcer la capacité de tir des ennemis
        AEnnemi.setNbTirsSimultanes(5);

        //créer les ennemis
        GroupeScene2 groupe = new GroupeScene2();
        int t = 360 / 24;
        Random rand = new Random();
        for (int i = 1; i <= 24; i++) {
            AEnnemi mobile = new EnnemiB(t * (Math.PI / 180), new Position(100, 100), this);
            IVue vue = i % 2 == 0 ? new SpriteA(this, mobile) : new SpriteB(this, mobile);

            //chacun porte un item par harsard
            IMobile itemM;
            IVue itemV;
            switch (rand.nextInt(3)) {
                case 0:
                    itemM = new MobileItem("missile", mobile.getPosition());
                    itemV = new SpriteItemMissile(this, itemM);
                    break;
                case 1:
                    itemM = new MobileItem("vie", mobile.getPosition());
                    itemV = new SpriteItemVie(this, itemM);
                    break;
                default:
                    itemM = new MobileItem("bomb", mobile.getPosition());
                    itemV = new SpriteItemBomb(this, itemM);
                    break;
            }
            mobile.ajouterItem(new Vaisseau(itemM, itemV));

            t += 360 / 24;
            groupe.ajouter(new Vaisseau(mobile, vue));
            nbEnnemiTotal++;
        }
        vaisseaux.add(groupe);

        // Restaurer le heros avec le nombre de vie restante
        ancienHeros.deplacerA(Const.FENETRE_LARGEUR / 2, Const.FENETRE_HAUTEUR - 100);
        herosMobile = new MobileHeros(ancienHeros, this);
        IVue herosVue = new SpriteHeros(this, herosMobile);
        vaisseaux.add(new Vaisseau(herosMobile, herosVue));
    }

    /**
     * Surcharger la méthode dessiner pour insérer l'affichage du nombre des ennemis tués
     */
    private void afficherScore() {
        f.textSize(40);
        f.text((nbEnnemiTotal - compterVaisseau()[1]) + "/" + nbEnnemiTotal, 100, 40);
    }

    @Override
    public IScene dessiner() {
        IScene resultat = super.dessiner();
        if (resultat != this)
            return resultat;

        //Afficher le score après l'introduction
        if (duree_compteur == DUREE_INTRO) afficherScore();

        return this;
    }

    @Override
    public IScene scenePause() {
        return new ScenePause(f, this);
    }

    @Override
    public IScene sceneSuivante() {
        int[] nb = compterVaisseau();

        if (nb[0] == 0) //le heros est mort
            return new SceneFinPartie(f, false);
        else if (vaisseaux.size() == nb[0]) //le heros est le seul vaisseau qui reste
            return new Scene3(f, herosMobile);

        return this;
    }
}
