package APP.controleur;

import CORE.controleur.IScene;
import CORE.controleur.SpriteCollection;
import processing.core.PImage;

public class SceneMenu implements IScene {

    private FenetreControleur f;
    private PImage nouveau, quitter, actif;
    private boolean entree = false;
    private boolean nouveauJeu = false;
    private boolean quitterJeu = false;

    public SceneMenu(FenetreControleur f) {
        this.f = f;
        nouveau = SpriteCollection.get("Accueil_Nouveau.png");
        quitter = SpriteCollection.get("Accueil_Quitter.png");
        actif = nouveau;
    }

    @Override
    public IScene dessiner() {
        f.image(actif, 0, 0);
        if ((entree && actif == nouveau) || nouveauJeu) {
            return new Scene1(f);
        } else if ((entree && actif == quitter) || quitterJeu) {
            System.exit(0);
        }
        return this;
    }

    @Override
    public void keyPressed() {
        if (f.keyPressed) {
            if (f.key == processing.core.PConstants.CODED) {
                if (f.keyCode == processing.core.PConstants.UP) {
                    actif = nouveau;
                } else if (f.keyCode == processing.core.PConstants.DOWN) {
                    actif = quitter;
                }
            } else if (f.keyCode == processing.core.PConstants.ENTER || f.keyCode == processing.core.PConstants.RETURN) {
                entree = true;
            } else if (f.key == 'n' || f.key == 'N') {
                nouveauJeu = true;
            } else if (f.key == 'q' || f.key == 'Q') {
                quitterJeu = true;
            }
        }
    }
}
