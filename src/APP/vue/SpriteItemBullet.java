package APP.vue;

import CORE.controleur.ASceneJeu;
import CORE.modele.IMobile;
import CORE.vue.ASprite;

public class SpriteItemBullet extends ASprite {
    public final static int LARGEUR = 19, HAUTEUR = 40;

    /**
     * Constructeur de la classe
     *
     * @param scene
     * @param mobile
     */
    public SpriteItemBullet(ASceneJeu scene, IMobile mobile) {
        super(scene, mobile);
        setSprite("bullet2_icon.png", LARGEUR, HAUTEUR);
        setContour("bullet2_icon_calque.png", LARGEUR, HAUTEUR);
    }
}
