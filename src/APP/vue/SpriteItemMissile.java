package APP.vue;

import CORE.controleur.ASceneJeu;
import CORE.modele.IMobile;
import CORE.vue.ASprite;

public class SpriteItemMissile extends ASprite {
    public final static int LARGEUR = 15, HAUTEUR = 40;

    /**
     * Constructeur de la classe
     *
     * @param scene
     * @param mobile
     */
    public SpriteItemMissile(ASceneJeu scene, IMobile mobile) {
        super(scene, mobile);
        setSprite("missile_icon.png", LARGEUR, HAUTEUR);
        setContour("missile_icon_calque.png", LARGEUR, HAUTEUR);
    }
}
