package APP.vue;

import CORE.controleur.ASceneJeu;
import CORE.modele.IMobile;
import CORE.vue.ASprite;

public class SpriteBullet2 extends ASprite {
    public static final int LARGEUR = 20, HAUTEUR = 42;

    /**
     * Constructeur de la classe
     *
     * @param scene
     * @param mobile
     */
    public SpriteBullet2(ASceneJeu scene, IMobile mobile) {
        super(scene, mobile);
        setSprite("bullet2.png", LARGEUR, HAUTEUR);
        setContour("bullet_calque.png", LARGEUR, HAUTEUR);
    }
}
