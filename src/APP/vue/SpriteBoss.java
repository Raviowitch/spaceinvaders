package APP.vue;

import CORE.controleur.ASceneJeu;
import CORE.modele.IMobile;
import CORE.vue.ASprite;

public class SpriteBoss extends ASprite {
    public static final int LARGEUR_BOSS = 208;
    public static final int HAUTEUR_BOSS = 208;

    private int vieTotale = 500;
    private int tailleBarre = 200;

    public SpriteBoss(ASceneJeu scene, IMobile mobile) {
        super(scene, mobile);
        setSprite("boss.png", LARGEUR_BOSS, HAUTEUR_BOSS);
        setContour("boss_calque.png", LARGEUR_BOSS, HAUTEUR_BOSS);
    }

    @Override
    public void dessiner() {
        super.dessiner();
        dessinerVie();
    }

    public void dessinerVie() {
        int taille = mobile.getVie() * tailleBarre / vieTotale;
        scene.fill(255, 0, 0);
        scene.textSize(20);
        scene.text("BOSS : ", 750, 30);
        scene.noFill();
        scene.stroke(255);
        scene.rect(795, 20, tailleBarre + 10, 30);
        scene.fill(255, 0, 0);
        scene.noStroke();
        scene.rect(800, 25, taille, 20);
    }
}
