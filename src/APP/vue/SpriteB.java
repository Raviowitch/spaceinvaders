package APP.vue;

import CORE.controleur.ASceneJeu;
import CORE.modele.IMobile;
import CORE.vue.ASprite;

public class SpriteB extends ASprite {
    public SpriteB(ASceneJeu scene, IMobile mobile) {
        super(scene, mobile);
        setSprite("ennemi2.png", 50, 50);
        setContour("ennemi_calque.png", 50, 50);
    }
}