package APP.vue;

import APP.modele.MobileHeros;
import CORE.controleur.ASceneJeu;
import CORE.controleur.Const;
import CORE.controleur.SpriteCollection;
import CORE.modele.IMobile;
import CORE.vue.ASprite;
import processing.core.PImage;

public class SpriteHeros extends ASprite {

    public static final int LARGEUR_HEROS = 70;
    private PImage coeur;
    private PImage[] armes;

    public SpriteHeros(ASceneJeu scene, IMobile mobile) {
        super(scene, mobile);
        setSprite("heros.png", LARGEUR_HEROS, LARGEUR_HEROS);
        setContour("heros_calque.png", LARGEUR_HEROS, LARGEUR_HEROS);
        this.coeur = SpriteCollection.get("coeur.png");
        this.armes = new PImage[]{
                SpriteCollection.get("bullet_icon.png"),
                SpriteCollection.get("bullet2_icon.png"),
                SpriteCollection.get("missile_icon.png"),
                SpriteCollection.get("bomb_atomic_icon.png")
        };
    }

    @Override
    public void dessiner() {
        super.dessiner();
        afficherVie();
        afficherArmes();
    }

    private void afficherVie() {
        for (int i = 1; i <= mobile.getVie(); i++) {
            scene.image(coeur, 20 * i, Const.FENETRE_HAUTEUR - 50);
        }
    }

    private void afficherArmes() {
        int armeCourante = ((MobileHeros) mobile).getArmeCourante();
        int qte = ((MobileHeros) mobile).getQteArmeCourante();
        scene.image(armes[armeCourante], Const.FENETRE_LARGEUR - 50, Const.FENETRE_HAUTEUR - 70);
        scene.textSize(20);
        scene.fill(255, 255, 255);
        scene.text(qte == -1 ? "∞" : String.valueOf(qte), Const.FENETRE_LARGEUR - 20, Const.FENETRE_HAUTEUR - 50);
    }
}
