package APP.vue;

import CORE.controleur.ASceneJeu;
import CORE.modele.IMobile;
import CORE.vue.ASprite;

public class SpriteA extends ASprite {
    public final static int LARGEUR = 50, HAUTEUR = 50;

    public SpriteA(ASceneJeu scene, IMobile mobile) {
        super(scene, mobile);
        setSprite("ennemi1.png", LARGEUR, HAUTEUR);
        setContour("ennemi_calque.png", LARGEUR, HAUTEUR);
    }
}
