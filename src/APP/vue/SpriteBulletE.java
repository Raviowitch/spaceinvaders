package APP.vue;

import CORE.controleur.ASceneJeu;
import CORE.modele.IMobile;
import CORE.vue.ASprite;

public class SpriteBulletE extends ASprite {

    public SpriteBulletE(ASceneJeu scene, IMobile mobile) {
        super(scene, mobile);
        setSprite("bullet_ennemi.png", 20, 28);
        setContour("bullet_ennemi_calque.png", 20, 28);
    }
}
