package APP.vue;

import CORE.controleur.ASceneJeu;
import CORE.modele.IMobile;
import CORE.vue.ASprite;

public class SpriteSimpsons extends ASprite {
    /**
     * Constructeur de la classe
     *
     * @param scene
     * @param mobile
     */
    public SpriteSimpsons(ASceneJeu scene, IMobile mobile) {
        super(scene, mobile);
        setSprite("simpsons.png", 100, 100);
        setContour("simpsons_calque.png", 100, 100);
    }
}
