package APP.vue;

import CORE.controleur.ASceneJeu;
import CORE.modele.IMobile;
import CORE.vue.ASprite;

public class SpriteItemVie extends ASprite {

    public final static int LARGEUR = 20, HAUTEUR = 17;

    /**
     * Constructeur de la classe
     *
     * @param scene
     * @param mobile
     */
    public SpriteItemVie(ASceneJeu scene, IMobile mobile) {
        super(scene, mobile);
        setSprite("coeur_icon.png", LARGEUR, HAUTEUR);
        setContour("coeur_icon_calque.png", LARGEUR, HAUTEUR);
    }
}
