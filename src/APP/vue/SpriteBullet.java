package APP.vue;

import CORE.controleur.ASceneJeu;
import CORE.modele.IMobile;
import CORE.vue.ASprite;

public class SpriteBullet extends ASprite {

    public static final int LARGEUR = 20, HAUTEUR = 42;

    public SpriteBullet(ASceneJeu scene, IMobile mobile) {
        super(scene, mobile);
        setSprite("bullet.png", LARGEUR, HAUTEUR);
        setContour("bullet_calque.png", LARGEUR, HAUTEUR);
    }
}
