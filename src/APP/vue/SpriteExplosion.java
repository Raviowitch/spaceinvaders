package APP.vue;

import CORE.controleur.ASceneJeu;
import CORE.modele.IMobile;
import CORE.vue.ASprite;

public class SpriteExplosion extends ASprite {

    public SpriteExplosion(ASceneJeu scene, IMobile mobile) {
        super(scene, mobile);
        setSprite("explosion.png", 84, 100);
        setContour("explosion_calque.png", 84, 100);
    }
}
