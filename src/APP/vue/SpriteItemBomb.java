package APP.vue;

import CORE.controleur.ASceneJeu;
import CORE.modele.IMobile;
import CORE.vue.ASprite;

public class SpriteItemBomb extends ASprite {
    public final static int LARGEUR = 15, HAUTEUR = 41;

    /**
     * Constructeur de la classe
     *
     * @param scene
     * @param mobile
     */
    public SpriteItemBomb(ASceneJeu scene, IMobile mobile) {
        super(scene, mobile);
        setSprite("bomb_atomic_icon.png", LARGEUR, HAUTEUR);
        setContour("bomb_atomic_icon_calque.png", LARGEUR, HAUTEUR);
    }
}