package APP.vue;

import CORE.controleur.ASceneJeu;
import CORE.modele.IMobile;
import CORE.vue.ASprite;

public class SpriteMur extends ASprite {
    public static final int LARGEUR = 18, HAUTEUR = 11;

    public SpriteMur(ASceneJeu fenetre, IMobile mobile) {
        super(fenetre, mobile);
        setSprite("brique.png", LARGEUR, HAUTEUR);
        setContour("brique_calque.png", LARGEUR, HAUTEUR);
    }

}


