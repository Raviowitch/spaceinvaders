package APP.vue;

import CORE.controleur.ASceneJeu;
import CORE.modele.IMobile;
import CORE.vue.ASprite;

public class SpriteBomb extends ASprite {

    public static final int LARGEUR = 32, HAUTEUR = 65;

    public SpriteBomb(ASceneJeu scene, IMobile mobile) {
        super(scene, mobile);
        setSprite("missile.png", LARGEUR, HAUTEUR);
        setContour("missile_calque.png", LARGEUR, HAUTEUR);
    }
}
