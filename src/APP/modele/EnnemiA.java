package APP.modele;

import APP.vue.SpriteBulletE;
import CORE.controleur.ASceneJeu;
import CORE.modele.AEnnemi;
import CORE.modele.Position;
import CORE.vaisseauxGraphiques.Vaisseau;

public class EnnemiA extends AEnnemi {

    /**
     * Constructeur
     *
     * @param position
     * @param scene
     */
    public EnnemiA(Position position, ASceneJeu scene) {
        super(position, scene, 2); //ma vie est 2
        this.deltaX = 1;
    }

    @Override
    public void tirer() {
        Position p = new Position(position.getX() + 25, position.getY());
        MobileBullet bullet = new MobileBullet(p, this, VITESSE_TIR, 1);
        SpriteBulletE balle = new SpriteBulletE(scene, bullet);
        scene.ajouterVaisseau(new Vaisseau(bullet, balle));
    }
}
