package APP.modele;

import APP.controleur.FenetreControleur;
import APP.vue.*;
import CORE.controleur.ASceneJeu;
import CORE.controleur.Const;
import CORE.modele.AHeros;
import CORE.modele.IMobile;
import CORE.modele.ITireur;
import CORE.modele.Position;
import CORE.vaisseauxGraphiques.Vaisseau;
import CORE.vue.IVue;
import processing.core.PConstants;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class MobileHeros extends AHeros {

    private FenetreControleur f;
    private int[] armes; //[0]: balle niveau 1; [1] balle niveau 2; [2] missile, [3] bomb; valeur: -1 pour illimité;
    private int armeCourante; //indice de l'arme courante

    /**
     * Constructeur
     *
     * @param position
     * @param scene
     * @param f
     */
    public MobileHeros(Position position, ASceneJeu scene, FenetreControleur f, int vie) {
        super(position, scene, 3);
        this.f = f;
        this.armes = new int[]{-1, 0, 0, 0};
        this.armeCourante = 0;
    }

    /**
     * Cloneur
     *
     * @param autreHeros
     */
    public MobileHeros(MobileHeros autreHeros, ASceneJeu scene) {
        super(autreHeros.getPosition(), scene, autreHeros.getVie());
        this.f = autreHeros.f;
        this.armes = autreHeros.armes;
        this.armeCourante = autreHeros.getArmeCourante();
    }

    /**
     * Récupérer la quantité de l'arme courante qui reste
     *
     * @return
     */
    public int getQteArmeCourante() {
        return this.armes[this.armeCourante];
    }

    /**
     * Récupérer l'indice de l'arme courante
     *
     * @return
     */
    public int getArmeCourante() {
        return this.armeCourante;
    }

    /**
     * Ajouter la gestion de la collision avec les items
     *
     * @param v Vaisseau heurté
     */
    @Override
    public void estTouche(Vaisseau v) {
        super.estTouche(v);

        //manger les armes/items
        if (v.est("item")) {
            MobileItem itemMobile = (MobileItem) v.getModele();
            if (itemMobile.getNom().equals("missile")) {
                armes[2]++;
            } else if (itemMobile.getNom().equals("bullet2")) {
                armes[1] = -1;
            } else if (itemMobile.getNom().equals("bomb")) {
                armes[3]++;
            } else if (itemMobile.getNom().equals("vie")) {
                vie++;
            }
        }
    }

    /**
     * Capturer les touches et changer la vitesse / l'arme correspondante
     */
    @Override
    protected void controleTouche() {
        if (f.keyPressed) {
            if (f.key == PConstants.CODED) {
                if (f.keyCode == PConstants.LEFT) {
                    //bouger à gauche
                    deltaX = -VITESSE;
                } else if (f.keyCode == PConstants.RIGHT) {
                    //bouger à droite
                    deltaX = VITESSE;
                }
            } else if (f.key == '\t') { //Tab
                //changer l'arme courante
                armeCourante = armeCourante < (armes.length - 1) ? armeCourante + 1 : 0;
                f.keyPressed = false;
            } else if (f.key == ' ') { //espace
                tirer();
                f.keyPressed = false;
            } else if (f.key == 'y' || f.key == 'Y') { //tricher
                this.armes = new int[]{-1, -1, 99, 99};
                this.vie = 46;
                f.keyPressed = false;
            }
        } else {
            if (f.key == PConstants.CODED) {
                if ((f.keyCode == PConstants.LEFT && deltaX == -VITESSE)
                        || (f.keyCode == PConstants.RIGHT && deltaX == VITESSE))
                    deltaX = 0;
            }
        }
    }

    /**
     * Tirer un missile
     */
    private void tirerMissile() {
        //Vérifier sa quantité
        if (armes[2] == 0)
            return;

        //Mettre à jour la quantité
        armes[2]--;

        //Créer 2 missiles
        Position p = new Position(position.getX() - SpriteBomb.LARGEUR, position.getY());
        IMobile bullet = new MobileMissile(p, this, -20, scene);
        IVue balle = new SpriteBomb(scene, bullet);
        scene.ajouterVaisseau(new Vaisseau(bullet, balle));

        p = new Position(position.getX() + SpriteHeros.LARGEUR_HEROS, position.getY());
        final IMobile finalBullet = new MobileMissile(p, this, -20, scene);
        final IVue finalBalle = new SpriteBomb(scene, finalBullet);
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                scene.ajouterVaisseau(new Vaisseau(finalBullet, finalBalle));
            }
        }, 100);
    }

    /**
     * Rirer une balle: niveau 1 ou 2
     */
    private void tirerBullet() {
        if (tirBloque || armes[armeCourante] == 0)
            return;

        //Créer la balle...
        Position p = new Position(position.getX() + SpriteHeros.LARGEUR_HEROS / 2 - SpriteBullet.LARGEUR / 2, position.getY());

        //...selon son niveau
        IMobile bullet = new MobileBullet(p, this, -10, armeCourante == 0 ? 1 : 3);
        IVue balle = armeCourante == 0 ? new SpriteBullet(scene, bullet) : new SpriteBullet2(scene, bullet);
        scene.ajouterVaisseau(new Vaisseau(bullet, balle));

        //bloquer le tir pour qu'une seule balle peut etre tirée à la fois
        tirBloque = true;
    }

    /**
     * Bombarder
     */
    private void tirerBombe() {
        if (armes[3] == 0)
            return;

        armes[3]--;

        Timer timer = new Timer();
        final ITireur self = this;

        //Créer 20 explosions aléatoires dans l'écran
        for (int i = 0; i < 20; i++) {
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    Random rand = new Random();
                    Position p = new Position(rand.nextInt(Const.FENETRE_LARGEUR - Const.FENETRE_BORDURE * 2) + Const.FENETRE_BORDURE,
                            rand.nextInt(Const.FENETRE_HAUTEUR / 2));
                    IMobile bullet = new MobileExplosion(p, self, 0);
                    IVue balle = new SpriteExplosion(scene, bullet);
                    scene.ajouterVaisseau(new Vaisseau(bullet, balle));
                }
            }, 250 * i);
        }
    }

    @Override
    public void tirer() {
        switch (armeCourante) {
            case 0:
            case 1:
                tirerBullet();
                break;
            case 2:
                tirerMissile();
                break;
            case 3:
                tirerBombe();
                break;
        }
    }
}
