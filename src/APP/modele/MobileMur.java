package APP.modele;

import CORE.modele.AMobile;
import CORE.modele.IMobile;
import CORE.modele.Position;

public class MobileMur extends AMobile {

    public MobileMur(Position p) {
        super(p, 0, 0, 1);
    }

    @Override
    public void deplacer() {
        //Le mur ne bouge pas
    }

    @Override
    public boolean peutToucher(IMobile sonMobile) {
        return sonMobile instanceof MobileBullet;
    }
}