package APP.modele;

import CORE.modele.Etat;
import CORE.modele.ITireur;
import CORE.modele.Position;
import CORE.vaisseauxGraphiques.Vaisseau;

import java.util.Timer;
import java.util.TimerTask;

public class MobileExplosion extends MobileBullet {

    MobileExplosion(Position position, ITireur tireur, int deltaY) {
        super(position, tireur, deltaY, 3);

        //L'explosion sera mort dans 100ms après son apparition
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                etat = Etat.MORT;
                vie = 0;
            }
        }, 100);
    }

    @Override
    public void estTouche(Vaisseau v) {
        //Pas de réaction pour cet évènement
    }

    @Override
    public void detruire() {
        //Pas de réaction pour cet évènement
    }
}
