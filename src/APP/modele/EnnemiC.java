package APP.modele;

import APP.vue.SpriteBulletE;
import CORE.controleur.ASceneJeu;
import CORE.controleur.Const;
import CORE.modele.AEnnemi;
import CORE.modele.Position;
import CORE.vaisseauxGraphiques.Vaisseau;

public class EnnemiC extends AEnnemi {
    private double t = 0.0;
    private double gap = -0.05;
    private int rayon = 200;
    private Position center;

    public EnnemiC(double t, Position position, ASceneJeu f) {
        super(position, f, 5); //ma vie est 5
        this.t = t;
        center = new Position(Const.FENETRE_LARGEUR / 2, Const.FENETRE_HAUTEUR / 3);
    }

    @Override
    public void deplacer() {
        super.tirVerifier();
        t += gap;
        position.setX((int) (center.getX() + (rayon * Math.cos(t))));
        position.setY((int) (center.getY() + (rayon * Math.sin(t))));
    }

    @Override
    public void deplacerA(int newX, int newY) {
        center.setX(newX);
        center.setY(newY);
    }

    @Override
    public void deplacer(int deltaX, int deltaY) {
        center.shiftX(deltaX);
        center.shiftY(deltaY);
    }

    @Override
    public void tirer() {
        Position p = new Position(position.getX() + 25, position.getY());
        MobileBullet bullet = new MobileBullet(p, this, VITESSE_TIR, 1);
        SpriteBulletE balle = new SpriteBulletE(scene, bullet);
        scene.ajouterVaisseau(new Vaisseau(bullet, balle));
    }

}
