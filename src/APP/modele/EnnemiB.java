package APP.modele;

import APP.vue.SpriteBulletE;
import CORE.controleur.ASceneJeu;
import CORE.controleur.Const;
import CORE.modele.AEnnemi;
import CORE.modele.Position;
import CORE.vaisseauxGraphiques.Vaisseau;

/**
 * Created by mathieulavergne on 16/02/2015.
 */
public class EnnemiB extends AEnnemi{

    /**
     * Constructeur
     *
     * @param position
     * @param scene
     */
    private double t;
    private double gap = 0.02;
    private Position center;

    public EnnemiB(double t, Position position, ASceneJeu scene) {
        super(position, scene, 3); //ma vie est 3
        this.t = t;
        center = new Position(Const.FENETRE_LARGEUR / 2, Const.FENETRE_HAUTEUR / 3);
    }

    @Override
    public void deplacer() {
        tirVerifier();
        t += gap;
        double x =  Math.cos(t) * 350 + center.getX();
        double y = 0.5 * Math.sin(t * 2) * 200 + center.getY();
        position.setX((int) x);
        position.setY((int) y);
    }

    @Override
    public void deplacer(int deltaX, int deltaY){
        center.shiftY(deltaY);
    }

    @Override
    public void tirer() {
        Position p = new Position(position.getX() + 25, position.getY());
        MobileBullet bullet = new MobileBullet(p, this, VITESSE_TIR, 1);
        SpriteBulletE balle = new SpriteBulletE(scene, bullet);
        scene.ajouterVaisseau(new Vaisseau(bullet, balle));
    }
}
