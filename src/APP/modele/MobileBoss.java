package APP.modele;

import APP.vue.SpriteBoss;
import APP.vue.SpriteBullet;
import APP.vue.SpriteBulletE;
import CORE.controleur.ASceneJeu;
import CORE.modele.AEnnemi;
import CORE.modele.Position;
import CORE.vaisseauxGraphiques.Vaisseau;

public class MobileBoss extends AEnnemi {

    public MobileBoss(Position position, ASceneJeu f) {
        super(position, f, 500); // ma vie = 500
        this.position = new Position(position.getX() - SpriteBoss.LARGEUR_BOSS / 2, position.getY() - SpriteBoss.HAUTEUR_BOSS / 2);
        this.compteurIntervalle = 25;
    }

    @Override
    public void deplacerA(int newX, int newY) {
        super.deplacerA(newX, newY);

        //Centrer le sprite après le déplacement
        deplacer(-SpriteBoss.LARGEUR_BOSS / 3, -SpriteBoss.HAUTEUR_BOSS / 3);
    }

    public void tirer() {
        //tirer 3 balles
        Position p = new Position(position.getX() - 5, position.getY() + SpriteBoss.HAUTEUR_BOSS - 55);
        MobileBullet bullet = new SuperBullet(p, this, 0, -1);
        SpriteBulletE balle = new SpriteBulletE(scene, bullet);
        scene.ajouterVaisseau(new Vaisseau(bullet, balle));

        Position p1 = new Position(position.getX() + SpriteBoss.LARGEUR_BOSS - SpriteBullet.LARGEUR,
                position.getY() + SpriteBoss.HAUTEUR_BOSS - 55);
        MobileBullet bullet1 = new MobileBullet(p1, this, VITESSE_TIR, 1);
        SpriteBulletE balle1 = new SpriteBulletE(scene, bullet1);
        scene.ajouterVaisseau(new Vaisseau(bullet1, balle1));

        Position p2 = new Position(position.getX() + SpriteBoss.LARGEUR_BOSS / 2 - SpriteBullet.LARGEUR / 2,
                position.getY() + SpriteBoss.HAUTEUR_BOSS);
        MobileBullet bullet2 = new SuperBullet(p2, this, 0, 1);
        SpriteBulletE balle2 = new SpriteBulletE(scene, bullet2);
        scene.ajouterVaisseau(new Vaisseau(bullet2, balle2));
    }
}