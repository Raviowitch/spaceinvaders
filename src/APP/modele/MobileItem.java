package APP.modele;

import CORE.modele.AItem;
import CORE.modele.Position;

public class MobileItem extends AItem {
    private String nom; //nom de l'item

    /**
     * Constructeur
     *
     * @param position
     */
    public MobileItem(String nom, Position position) {
        super(position);
        this.nom = nom;
    }

    /**
     * Récupérer le nom de l'item
     * @return
     */
    public String getNom() {
        return this.nom;
    }
}
