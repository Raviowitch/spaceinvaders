package APP.modele;

import APP.vue.SpriteExplosion;
import CORE.controleur.ASceneJeu;
import CORE.modele.ITireur;
import CORE.modele.Position;
import CORE.vaisseauxGraphiques.Vaisseau;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class MobileMissile extends MobileBullet {
    private ASceneJeu scene;
    private int compteur;
    private int t = 1;

    MobileMissile(Position position, ITireur tireur, int deltaY, ASceneJeu scene) {
        super(position, tireur, deltaY, 0);
        this.scene = scene;
        this.compteur = 0;
    }

    @Override
    public void detruire() {
        //si il est déjà détruit
        if (vie-- <= 0)
            return;

        //Créer les explosions
        Timer timer = new Timer();
        for (int i = 0; i < 5; i++) {
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    int deltaX = 0, deltaY = 0;
                    Random rand = new Random();
                    switch (compteur++) {
                        case 0:
                            deltaX = -10;
                            deltaY = -10;
                            break;
                        case 4:
                            tireur.debloquerTir();
                        default:
                            deltaX = rand.nextInt(75) - 25;
                            deltaY = rand.nextInt(75) - 25;
                    }
                    exploser(deltaX, deltaY);

                }
            }, (new Random().nextInt(100) + 300) * i);
        }
    }

    /**
     * Créer 5 explosions autour de l'endroit de collision
     *
     * @param deltaX
     * @param deltaY
     */
    private void exploser(int deltaX, int deltaY) {
        Position p = new Position(position.getX() + deltaX, position.getY() + deltaY);
        MobileExplosion bullet = new MobileExplosion(p, tireur, 0);
        SpriteExplosion balle = new SpriteExplosion(scene, bullet);
        scene.ajouterVaisseau(new Vaisseau(bullet, balle));
    }

    /**
     * Calculer le déplacement élastique du missile
     *
     * @param t
     * @return
     */
    public double easing(int t) {
        double ty = (double) t / (double) 40;
        return deltaY * ty * ty * ty * ty * ty + 1.2;
    }

    @Override
    public void deplacer() {
        position.shiftY((int) easing(t++));
    }
}
