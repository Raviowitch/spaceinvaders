package APP.modele;

import CORE.modele.ABullet;
import CORE.modele.ITireur;
import CORE.modele.Position;
import CORE.vaisseauxGraphiques.Vaisseau;

/**
 * Une balle générale
 */
public class MobileBullet extends ABullet {

    public MobileBullet(Position position, ITireur tireur, int deltaY, int degat) {
        super(position, tireur, degat);
        this.deltaY = deltaY;
    }

    @Override
    public void deplacer() {
        position.shiftY(deltaY);
    }
}
