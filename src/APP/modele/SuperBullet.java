package APP.modele;

import CORE.modele.ITireur;
import CORE.modele.Position;

import java.util.Random;

public class SuperBullet extends MobileBullet {

    private double t = 0.0;
    private double c = 0.05;
    private int direction;
    private int x0, y0;
    private int rayon;

    public SuperBullet(Position position, ITireur tireur, int deltaY, int direction) {
        super(position, tireur, deltaY, 1);
        x0 = position.getX();
        y0 = position.getY();
        this.direction = direction;
        this.rayon = new Random().nextInt(100) + 200;
    }

    @Override
    public void deplacer() {
        t += c;
        position = new Position(
                (int) (Math.sin(t) * rayon + x0) * direction,
                (int) (y0 + 100 * t)
        );
    }
}
