package APP.modele;

import APP.vue.SpriteBulletE;
import CORE.controleur.ASceneJeu;
import CORE.controleur.Const;
import CORE.modele.AEnnemi;
import CORE.modele.Position;
import CORE.vaisseauxGraphiques.Vaisseau;

public class EnnemiD extends AEnnemi {
    private double t = 0.0;
    private double gap = +0.05;
    private Position center;

    public EnnemiD(double t, Position position, ASceneJeu f) {
        super(position, f, 5); // ma vie est 5
        this.t = t;
        this.center = new Position(Const.FENETRE_LARGEUR / 2, Const.FENETRE_HAUTEUR / 3);
    }

    @Override
    public void deplacer() {
        super.tirVerifier();
        t += gap;
        double x = 0.5 * Math.sin(t * 2) * 150 + center.getX();
        double y = Math.cos(t) * 100 + center.getY();
        position.setX((int) x);
        position.setY((int) y);
    }

    @Override
    public void tirer() {
        Position p = new Position(position.getX() + 25, position.getY());
        MobileBullet bullet = new MobileBullet(p, this, VITESSE_TIR, 1);
        SpriteBulletE balle = new SpriteBulletE(scene, bullet);
        scene.ajouterVaisseau(new Vaisseau(bullet, balle));
    }

    @Override
    public void deplacerA(int newX, int newY) {
        center.setX(newX);
        center.setY(newY);
    }
}