package APP.modele;

import CORE.controleur.ASceneJeu;
import CORE.controleur.Const;
import CORE.modele.AEnnemi;
import CORE.modele.Etat;
import CORE.modele.Position;

public class EnnemiSpecial extends AEnnemi {
    /**
     * Constructeur
     *
     * @param position
     * @param scene
     * @param deltaX
     */
    public EnnemiSpecial(Position position, ASceneJeu scene, int deltaX) {
        super(position, scene, 1); //ma vie est 1
        this.deltaX = deltaX;
    }

    @Override
    public void tirer() {
        //cet ennemi ne peut pas tirer
    }

    @Override
    public void detruire() {
        //Si c'est une auto-destruction: ne pas jeter l'item emmené
        if (position.getX() < 0 || position.getX() > Const.FENETRE_LARGEUR)
            return;

        super.detruire();
    }

    @Override
    public void deplacer() {
        super.deplacer();

        //Quand il arrive à la destination précise, il sera détruit
        if ((position.getX() == Const.FENETRE_LARGEUR * 1.5 && deltaX > 0)
                || (position.getX() == -Const.FENETRE_LARGEUR / 2 && deltaX < 0))
            etat = Etat.MORT;
    }
}
