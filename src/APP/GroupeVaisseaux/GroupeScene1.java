package APP.GroupeVaisseaux;

import CORE.controleur.Const;
import CORE.vaisseauxGraphiques.GroupeVaisseau;

public class GroupeScene1 extends GroupeVaisseau {
    @Override
    public void deplacer() {
        //Quand on touche un des deux bords de l'écran,
        //on déplace le groupe de vaisseau en Y.
        if (getDroite() >= Const.FENETRE_LARGEUR - Const.FENETRE_BORDURE
                || getGauche() <= Const.FENETRE_BORDURE) {
            changerDirectionH();
        }
        super.deplacer();
    }
}
