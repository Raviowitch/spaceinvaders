package APP.GroupeVaisseaux;

import CORE.controleur.Const;
import CORE.vaisseauxGraphiques.GroupeVaisseau;

public class GroupeScene2 extends GroupeVaisseau{

    int deltaY = 5;
    @Override
    public void deplacer() {
        //Quand on touche un des deux bords de l'écran,
        //on déplace le groupe de vaisseau en Y.
        super.deplacer();
        deplacer(0,deltaY);
        if ((getBas() > Const.FENETRE_HAUTEUR - 200 && deltaY > 0) || (getHaut() < 0 && deltaY < 0)) {
            deltaY *= -1;
        }

    }
}
