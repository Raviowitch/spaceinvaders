package APP.GroupeVaisseaux;

import CORE.controleur.Const;
import CORE.vaisseauxGraphiques.GroupeVaisseau;

public class GroupeBoss extends GroupeVaisseau {
    double t = 0.0;
    double gap = +0.05;
    int rayon = 200;

    @Override
    public void deplacer() {
        super.deplacer();

        //Bouger en figure oval
        t += gap;
        int x = (int) ((Const.FENETRE_LARGEUR / 3 + (7 / 3) * (rayon * Math.cos(t)))) + 60;
        int y = (int) ((Const.FENETRE_HAUTEUR / 4 + (3 / 2) * (rayon * Math.sin(t))) / 2);
        deplacerA(x, y);
    }

    @Override
    public void deplacer(int deltaX, int deltaY) {
        super.deplacer(deltaX, deltaY);
        super.deplacer();
    }
}
