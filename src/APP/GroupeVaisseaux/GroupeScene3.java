package APP.GroupeVaisseaux;

import CORE.controleur.Const;
import CORE.vaisseauxGraphiques.GroupeVaisseau;

public class GroupeScene3 extends GroupeVaisseau {
    private final int BOUGER_Y_MAX = 50;
    private int BOUGER_Y = 1;
    private int compteurBouger = -1;

    @Override
    public void deplacer() {
        //Quand on touche un des deux bords de l'écran,
        //on déplace le groupe de vaisseau en Y.
        if (getDroite() >= Const.FENETRE_LARGEUR - Const.FENETRE_BORDURE
                || getGauche() <= Const.FENETRE_BORDURE) {
            if (compteurBouger == -1) {
                changerDirectionH();
                compteurBouger = 0;
                BOUGER_Y *= ((BOUGER_Y < 0 && getHaut() > 100)
                        || (BOUGER_Y > 0 && getBas() < Const.FENETRE_HAUTEUR * 2 / 3)) ? 1 : -1;
            } else if (compteurBouger == BOUGER_Y_MAX)
                compteurBouger = -1;
        }

        if (compteurBouger > -1) {
            deplacer(0, BOUGER_Y);
            compteurBouger++;
        } else {
            super.deplacer();
        }
    }
}
