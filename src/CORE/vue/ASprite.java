package CORE.vue;

import CORE.controleur.ASceneJeu;
import CORE.controleur.Const;
import CORE.controleur.SpriteCollection;
import CORE.modele.Etat;
import CORE.modele.IMobile;
import CORE.modele.Position;
import processing.core.PConstants;
import processing.core.PImage;

/**
 * Implémentation de l'interface IVue
 */
public abstract class ASprite implements IVue {
    protected IMobile mobile; //le modèle avec lequel on récupère l'état du mobile
    protected ASceneJeu scene; //sert à afficher/dessiner le sprite du mobile
    protected PImage sprite; //sprite principal
    protected PImage spriteTouche; //sprite représentant l'état "touché" du mobile
    protected PImage contour; //sprite spécial qui sert à vérifier la collision

    /**
     * Constructeur de la classe
     *
     * @param scene
     * @param mobile
     */
    public ASprite(ASceneJeu scene, IMobile mobile) {
        this.scene = scene;
        this.mobile = mobile;
    }

    /**
     * Initialisation des sprites
     *
     * @param image
     * @param largeur
     * @param hauteur
     */
    public void setSprite(String image, int largeur, int hauteur) {
        //récupérer le sprite à partir de la collection (préchargée)
        sprite = SpriteCollection.get(image);

        //redimensionner le sprite
        sprite.resize(largeur, hauteur);

        try {
            //cloner le sprite original et activer le filtrage
            //pour créer l'image "touché" du mobile
            spriteTouche = (PImage) sprite.clone();
            spriteTouche.filter(PConstants.INVERT);

        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Initialiser le contour
     *
     * @param image
     * @param largeur
     * @param hauteur
     */
    public void setContour(String image, int largeur, int hauteur) {
        //récupérer le sprite à partir de la collection (préchargée)
        contour = SpriteCollection.get(image);

        //redimensionner le sprite
        contour.resize(largeur, hauteur);
    }

    /**
     * Dessiner le sprite
     */
    @Override
    public void dessiner() {
        //Récupérer la position du mobile
        Position p = mobile.getPosition();

        //Dessiner le sprite selon l'état du mobile
        if (mobile.getEtat() == Etat.TOUCHE)
            scene.image(spriteTouche, p.getX(), p.getY());
        else
            scene.image(sprite, p.getX(), p.getY());
    }

    /**
     * Vérifier la collision avec un autre mobile
     *
     * @param sonMobile
     * @param saVue
     * @return Cette méthode rendra true
     * si et seulement si la collision rectangle vs rectangle
     * et la collision par pixel sont reconnues à la fois
     */
    @Override
    public boolean collisionPix(IMobile sonMobile, IVue saVue) {
        // Récupérer les coordonnées
        int monGauche = getGauche();
        int monDroite = getDroite();
        int monHaut = getHaut();
        int monBas = getBas();

        int sonGauche = saVue.getGauche();
        int sonDroite = saVue.getDroite();
        int sonHaut = saVue.getHaut();
        int sonBas = saVue.getBas();

        //Collision rectangle vs rectangle
        if (monGauche >= sonDroite || monDroite <= sonGauche || monHaut >= sonBas || monBas <= sonHaut)
            return false;

        //Collision pixel par pixel
        int blanc = scene.color(255);
        for (int j = sonHaut; j < sonBas; j++) {
            for (int i = sonGauche; i < sonDroite; i++) {
                int sonXPix = i - sonGauche;
                int sonYPix = j - sonHaut;
                int monXPix = i - monGauche;
                int monYPix = j - monHaut;
                if (getContour().get(monXPix, monYPix) == blanc && ((ASprite) saVue).getContour().get(sonXPix, sonYPix) == blanc) {
                    return true;
                }
            }
        }
        return false;
    }
    /*
    LES GETTERS
     */

    public PImage getContour() {
        return contour;
    }

    @Override
    public int getLargeur() {
        return sprite.width;
    }

    @Override
    public int getHauteur() {
        return sprite.height;
    }

    @Override
    public int getGauche() {
        return mobile.getPosition().getX();
    }

    @Override
    public int getDroite() {
        return getGauche() + getLargeur();
    }

    @Override
    public int getHaut() {
        return mobile.getPosition().getY();
    }

    @Override
    public int getBas() {
        return getHaut() + getHauteur();
    }
}