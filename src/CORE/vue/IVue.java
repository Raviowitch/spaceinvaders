package CORE.vue;

import CORE.modele.IMobile;

/**
 * Interface représentant l'affichage graphique des mobiles
 */
public interface IVue {

    /**
     * Dessiner le mobile
     */
    public void dessiner();

    /**
     * Vérifier la collision entre les deux mobiles
     *
     * @param sonMobile
     * @param saVue
     * @return
     */
    public boolean collisionPix(IMobile sonMobile, IVue saVue);

    /*
    GETTERS DE DIMENSION
     */
    public int getLargeur();

    public int getHauteur();

    public int getGauche();

    public int getDroite();

    public int getHaut();

    public int getBas();
}
