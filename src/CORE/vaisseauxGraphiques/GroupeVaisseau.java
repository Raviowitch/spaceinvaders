package CORE.vaisseauxGraphiques;

import CORE.controleur.Const;
import CORE.modele.Etat;
import CORE.modele.IMobile;
import CORE.modele.Position;
import CORE.vue.IVue;

import java.util.ArrayList;

/**
 * Classe représentant un groupe de vaisseaux
 */
public class GroupeVaisseau extends AVaisseau {
    private ArrayList<Vaisseau> vaisseaux;

    /**
     * Constructeur
     */
    public GroupeVaisseau() {
        vaisseaux = new ArrayList<Vaisseau>();
    }

    /*
    GESTION DE LA COLLECTION
     */

    /**
     * Ajouter un vaisseau au groupe
     *
     * @param v
     */
    public void ajouter(Vaisseau v) {
        vaisseaux.add(v);
    }

    /**
     * Enlever un vaisseau du groupe
     *
     * @param v
     */
    public void supprimer(Vaisseau v) {
        vaisseaux.remove(v);
    }

    /**
     * Retourne true si le vaisseau est dans la collection
     *
     * @param v
     * @return
     */
    public boolean contient(Vaisseau v) {
        return vaisseaux.contains(v);
    }

    /**
     * Retourne la taille du groupe de vaisseau
     *
     * @return
     */
    public int taille() {
        return vaisseaux.size();
    }

    /**
     * Retourne la collection des vaisseaux du groupe
     *
     * @return
     */
    public ArrayList<Vaisseau> getCollection() {
        return vaisseaux;
    }

    /**
     * Retourne un vaisseau dans le groupe
     *
     * @param index indice du vaisseau dans la collection
     * @return
     */
    public Vaisseau getVaisseau(int index) {
        return vaisseaux.get(index);
    }

    /*
    LES METHODES IMPLEMENTEES
     */

    @Override
    public boolean collisionTest(AVaisseau vaisseau) {
        boolean resultat = false;
        for (int i = 0; i < vaisseaux.size(); i++) {
            Vaisseau membre = vaisseaux.get(i);
            if (vaisseau.collisionTest(membre)) {
                resultat = true;
                if (membre.getEtat() == Etat.MORT) {
                    membre.detruire();
                    vaisseaux.remove(membre);
                    i--;
                }
            }
        }
        return resultat;
    }

    @Override
    public void deplacer() {
        for (Vaisseau vaisseau : vaisseaux) {
            vaisseau.deplacer();
        }
    }

    @Override
    public void deplacer(int deltaX, int deltaY) {
        for (Vaisseau vaisseau : vaisseaux) {
            vaisseau.deplacer(deltaX, deltaY);
        }
    }

    @Override
    public void deplacerA(int newX, int newY) {
        for (Vaisseau vaisseau : vaisseaux) {
            vaisseau.deplacerA(newX, newY);
        }
    }

    @Override
    public void changerDirectionH() {
        for (Vaisseau vaisseau : vaisseaux) {
            vaisseau.changerDirectionH();
        }
    }

    @Override
    public void changerDirectionV() {
        for (Vaisseau vaisseau : vaisseaux) {
            vaisseau.changerDirectionV();
        }
    }

    @Override
    public Position getPosition() {
        return new Position(getGauche(), getHaut());
    }

    @Override
    public void dessiner() {
        for (Vaisseau vaisseau : vaisseaux) {
            vaisseau.dessiner();
        }
    }

    @Override
    public boolean collisionPix(IMobile sonMobile, IVue saVue) {
        return false;
    }

    @Override
    public Etat getEtat() {
        return vaisseaux.size() > 0 ? Etat.ENVIE : Etat.MORT;
    }

    @Override
    public int getVie() {
        return vaisseaux.size();
    }

    @Override
    public void estTouche(Vaisseau v) {
        //do nothing
    }

    @Override
    public void detruire() {
        vaisseaux.clear();
    }

    @Override
    public boolean est(String type) {
        return type == "groupe";
    }

    /**
     * POUR LE CALCUL DES DIMENSIONS
     */

    @Override
    public int getGauche() {
        int xMin = Const.FENETRE_LARGEUR;
        for (Vaisseau vaisseau : vaisseaux) {
            int x = vaisseau.getPosition().getX();
            if (x < xMin)
                xMin = x;
        }
        return xMin;
    }

    @Override
    public int getDroite() {
        int xMax = 0;
        for (Vaisseau vaisseau : vaisseaux) {
            int x = vaisseau.getPosition().getX() + vaisseau.getLargeur();
            if (x > xMax)
                xMax = x;
        }
        return xMax;
    }

    @Override
    public int getHaut() {
        int yMin = Const.FENETRE_HAUTEUR;
        for (Vaisseau vaisseau : vaisseaux) {
            int y = vaisseau.getPosition().getY();
            if (y < yMin)
                yMin = y;
        }
        return yMin;
    }

    @Override
    public int getBas() {
        int yMax = 0;
        for (Vaisseau vaisseau : vaisseaux) {
            int y = vaisseau.getPosition().getY() + vaisseau.getHauteur();
            if (y > yMax)
                yMax = y;
        }
        return yMax;
    }

    @Override
    public int getLargeur() {
        return this.getDroite() - this.getGauche();
    }

    @Override
    public int getHauteur() {
        return this.getBas() - this.getHaut();
    }

}
