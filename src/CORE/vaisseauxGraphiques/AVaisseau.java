package CORE.vaisseauxGraphiques;

import CORE.modele.IMobile;
import CORE.vue.IVue;

/**
 * Classe abstraite qui implémente IVue et IMobile.
 * Sert à représenter les vaisseaux en général (soit vaisseau, soit groupe de vaisseaux)
 */
public abstract class AVaisseau implements IVue, IMobile {

    @Override
    public abstract void deplacer();

    @Override
    public abstract void dessiner();

    /**
     * Vérifier le type du vaisseau (modele)
     *
     * @param type
     * @return
     */
    public abstract boolean est(String type);

    /**
     * Vérifier la collision vaisseau vs vaisseau et
     * vaisseau vs groupe de vaisseaux
     *
     * @param vaisseau
     * @return
     */
    public abstract boolean collisionTest(AVaisseau vaisseau);


}
