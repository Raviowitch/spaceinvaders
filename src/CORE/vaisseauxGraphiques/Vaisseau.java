package CORE.vaisseauxGraphiques;

import CORE.modele.*;
import CORE.vue.IVue;

public class Vaisseau extends AVaisseau {
    private IMobile mobile;
    private IVue vue;

    public Vaisseau(IMobile mobile, IVue vue) {
        this.mobile = mobile;
        this.vue = vue;
    }

    /**
     * Vérifier la possibilité de la collision
     *
     * @param v
     * @return true si la collision entre ces 2 types de mobile est possible
     */
    public boolean peutToucher(Vaisseau v) {
        return ((AMobile) mobile).peutToucher(v.getModele());
    }

    @Override
    public boolean collisionTest(AVaisseau v) {
        //si le cible est un groupe de vaisseaux...
        if (v instanceof GroupeVaisseau)
            return v.collisionTest(this);

        boolean resultat = false;

        //si la collision est possible + s'est faite...
        if (peutToucher((Vaisseau) v) && collisionPix(((Vaisseau) v).getModele(), ((Vaisseau) v).getVue())) {

            //invoquer l'événement
            v.estTouche(this);
            this.estTouche((Vaisseau) v);

            //affecter le résultat
            resultat = true;
        }

        return resultat;
    }

    @Override
    public boolean est(String type) {
        if (type.equals("balle"))
            return mobile instanceof ABullet;
        else if (type.equals("ennemi"))
            return mobile instanceof AEnnemi;
        else if (type.equals("heros"))
            return mobile instanceof AHeros;
        else if (type.equals("item"))
            return mobile instanceof AItem;
        return false;
    }

    public IMobile getModele() {
        return mobile;
    }

    public IVue getVue() {
        return vue;
    }

    @Override
    public void deplacer() {
        mobile.deplacer();
    }

    @Override
    public void deplacer(int deltaX, int deltaY) {
        mobile.deplacer(deltaX, deltaY);
    }

    @Override
    public void deplacerA(int newX, int newY) {
        mobile.deplacerA(newX, newY);
    }

    @Override
    public void dessiner() {
        vue.dessiner();
    }

    @Override
    public Position getPosition() {
        return mobile.getPosition();
    }

    @Override
    public Etat getEtat() {
        return mobile.getEtat();
    }

    @Override
    public int getVie() {
        return mobile.getVie();
    }

    @Override
    public void estTouche(Vaisseau v) {
        mobile.estTouche(v);
    }

    @Override
    public void detruire() {
        mobile.detruire();
    }

    @Override
    public void changerDirectionH() {
        mobile.changerDirectionH();
    }

    @Override
    public void changerDirectionV() {
        mobile.changerDirectionV();
    }

    @Override
    public boolean collisionPix(IMobile sonMobile, IVue saVue) {
        return vue.collisionPix(sonMobile, saVue);
    }

    @Override
    public int getLargeur() {
        return vue.getLargeur();
    }

    @Override
    public int getHauteur() {
        return vue.getHauteur();
    }

    @Override
    public int getGauche() {
        return vue.getGauche();
    }

    @Override
    public int getDroite() {
        return vue.getDroite();
    }

    @Override
    public int getHaut() {
        return vue.getHaut();
    }

    @Override
    public int getBas() {
        return vue.getBas();
    }
}