package CORE.controleur;

/**
 * Les constantes utilisées dans le programme
 */
public class Const {
    public static final int FENETRE_LARGEUR = 1024;
    public static final int FENETRE_HAUTEUR = 768;
    public static final int FENETRE_BORDURE = 10;
}