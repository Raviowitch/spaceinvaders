package CORE.controleur;

import APP.controleur.FenetreControleur;
import processing.core.PImage;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Hashtable;

/**
 * La classe statique qui sert à stocker les images
 * afin d'accélérer l'initialisation des objets graphiques
 */
public class SpriteCollection {
    //Initialiser le dictionnaire des sprites
    private static Hashtable<String, PImage> sprites = new Hashtable<String, PImage>();
    private static File[] imgs = new File[]{};

    //Indicateur
    private static boolean estInitialise = false;

    /**
     * Initialiser la collection
     *
     * @param f
     */
    public static void init(FenetreControleur f) {
        //Récupérer le chemin du répertoire des images
        String folder = SpriteCollection.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        folder += "images";

        //Décoder le chemin
        try {
            folder = URLDecoder.decode(folder, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        //Récupérer toutes les images dans le répertoire
        imgs = new File(folder).listFiles();

        //Charger les images et les ajouter dans le dictionnaire:
        //chaque enregistrement prend le nom du fichier comme clé
        //et l'objet correspondant de type PImage comme valeur
        for (File img : imgs)
            sprites.put(img.getName(), f.loadImage(img.toString()));

        //Indiquer que la collection est initialisée
        estInitialise = true;
    }

    /**
     * Récupérer l'image avec son nom (clé)
     *
     * @param name
     * @return
     */
    public static PImage get(String name) {
        return sprites.get(name);
    }

    /**
     * Calculer le progrès du chargement des images
     *
     * @return la valeur entre 0.0 et 1.0
     */
    public static double getProgress() {
        return (double) sprites.size() / (double) imgs.length;
    }

    /**
     * Verifier si le dictionnaire est déjà initialisé
     *
     * @return
     */
    public static boolean estInitialise() {
        return estInitialise;
    }
}
