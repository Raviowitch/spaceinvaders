package CORE.controleur;

import APP.controleur.FenetreControleur;
import CORE.modele.AEnnemi;
import CORE.modele.Etat;
import CORE.vaisseauxGraphiques.AVaisseau;
import CORE.vaisseauxGraphiques.GroupeVaisseau;
import CORE.vaisseauxGraphiques.Vaisseau;
import processing.core.PConstants;
import processing.core.PImage;

import java.util.ArrayList;

public abstract class ASceneJeu implements IScene {
    //Pour le dessin de l'introduction
    protected int DUREE_INTRO = 120; //120 frames = 2s
    protected int duree_compteur = 0;

    protected FenetreControleur f;//l'instance de la fenetre controleur
    protected boolean estPause = false;//indicateur de la pause
    protected PImage fond;//image de fond
    protected String nomScene; //nom de la scene

    //collection des vaisseaux
    protected ArrayList<AVaisseau> vaisseaux = new ArrayList<AVaisseau>();
    protected ArrayList<AVaisseau> vaisseauxAAjouter = new ArrayList<AVaisseau>();
    protected ArrayList<AVaisseau> vaisseauxASupprimer = new ArrayList<AVaisseau>();

    //Constructeur
    public ASceneJeu(String nomScene, FenetreControleur f, String nomFond) {
        this.f = f;
        this.fond = SpriteCollection.get(nomFond);
        this.nomScene = nomScene;
        AEnnemi.resetTirs();
    }

    @Override
    public IScene dessiner() {
        //dessiner l'introduction
        if (duree_compteur < DUREE_INTRO) {
            duree_compteur++;
            dessinerIntro();
            return this;
        }

        // fond
        f.image(fond, 0, 0);

        // vaisseaux : suppression
        if (vaisseauxASupprimer.size() > 0) {
            vaisseaux.removeAll(vaisseauxASupprimer);
            vaisseauxASupprimer.clear();
        }

        // vaisseaux : ajout
        if (vaisseauxAAjouter.size() > 0) {
            vaisseaux.addAll(vaisseauxAAjouter);
            vaisseauxAAjouter.clear();
        }

        // vaisseaux : deplacement et dessin
        for (AVaisseau v : vaisseaux) {
            v.dessiner();
            v.deplacer();
        }


        //Verifier la collision
        controleCollision();

        //Nettoyer les vaisseaux qui ne sont plus necessaires
        nettoyer();

        //verifie si le jeu est en pause (touche p)
        if (estPause) {
            estPause = false;
            return scenePause();
        }

        return sceneSuivante();
    }

    /**
     * Capturer les touches
     */
    @Override
    public void keyPressed() {
        if (f.key == 'p' || f.key == 'P') { //PAUSE
            estPause = true;
        } else if (f.key == 't' || f.key == 'T') { //ELIMINER TOUS LES ENNEMIS
            //éliminer tous les vaisseaux ennemis
            for(int i = 0; i < vaisseaux.size(); i++){
                if (vaisseaux.get(i).est("ennemi") || vaisseaux.get(i).est("groupe"))
                {
                    vaisseaux.remove(i);
                    i--;
                }
            }
        }
    }

    /**
     * Retourner la scène pause
     *
     * @return
     */
    public abstract IScene scenePause();

    /**
     * Retourner la scène suivante
     *
     * @return
     */
    public abstract IScene sceneSuivante();

    /**
     * Compter le nombre des vaisseaux qui restent dans la scene
     *
     * @return [0]: nombre des heros, [1]: nombre des ennemis
     */
    public int[] compterVaisseau() {
        int nbHero = 0, nbEnnemi = 0;
        for (AVaisseau v : vaisseaux) {
            if (v.est("heros")) nbHero++;
            else if (v.est("ennemi")) nbEnnemi++;
            else if (v.est("groupe"))
                for (Vaisseau membre : ((GroupeVaisseau) v).getCollection()) {
                    if (membre.est("ennemi")) nbEnnemi++;
                }
        }
        return new int[]{nbHero, nbEnnemi};
    }

    /**
     * Vérifier si un vaisseau est toujours dans la scene
     *
     * @param cible
     * @return
     */
    public boolean contient(Vaisseau cible) {
        if (vaisseaux.contains(cible)) return true;

        for (AVaisseau v : vaisseaux) {
            if (v.est("groupe") && ((GroupeVaisseau) v).contient(cible))
                return true;
        }

        return false;
    }

    /**
     * Dessiner l'introduction de la scène.
     * Cette méthode peut etre surchargée
     * pour faire quelque chose plus spéciale
     */
    protected void dessinerIntro() {
        f.background(0);
        f.fill(255);
        f.textSize(30);
        f.textAlign(PConstants.CENTER, PConstants.CENTER);
        f.text(nomScene, Const.FENETRE_LARGEUR / 2, Const.FENETRE_HAUTEUR / 2);
    }

    public void ajouterVaisseau(Vaisseau v) {
        vaisseauxAAjouter.add(v);
    }

    /**
     * Permet de vérifier pour chaque vaisseau s'il doit être supprimé
     */
    protected void nettoyer() {
        for (AVaisseau v : vaisseaux) {
            // si c'est un vaisseau mort
            if (v.getEtat() == Etat.MORT
                    //ou c'est une balle qui sort de l'écran
                    || (v.est("balle") && (v.getBas() < 0 || v.getHaut() > Const.FENETRE_HAUTEUR))) {
                v.detruire();
                vaisseauxASupprimer.add(v);
            }
        }
    }

    /**
     * Parcours les vaisseaux et verifie les collisions
     */
    protected void controleCollision() {
        for (int i = 0; i < vaisseaux.size(); i++) {
            AVaisseau v1 = vaisseaux.get(i);

            for (int j = i + 1; j < vaisseaux.size(); j++) {
                AVaisseau v2 = vaisseaux.get(j);
                v2.collisionTest(v1);
            }
        }
    }

    /**
     * Les wrappers des methodes de dessin (Processing)
     */

    public void image(PImage image, int x, int y) {
        f.image(image, x, y);
    }

    public int color(int i) {
        return f.color(i);
    }

    public void noFill() {
        f.noFill();
    }

    public void text(String s, int i, int j) {
        f.text(s, i, j);
    }

    public void textSize(float i){
        f.textSize(i);
    }

    public void stroke(int i) {
        f.stroke(i);
    }

    public void rect(int x, int y, int l, int h) {
        f.rect(x, y, l, h);
    }

    public void fill(int i, int i1, int i2) {
        f.fill(i, i1, i2);
    }

    public void noStroke() {
        f.noStroke();
    }
}
