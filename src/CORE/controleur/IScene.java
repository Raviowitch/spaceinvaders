package CORE.controleur;

/**
 * Interface représentant la scène
 */
public interface IScene {
    /**
     * Dessiner la scène et retourner la scène suivante
     *
     * @return
     */
    public IScene dessiner();

    /**
     * Pour traiter les touches capturées à partir du Applet
     */
    public void keyPressed();
}
