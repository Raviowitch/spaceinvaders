package CORE.modele;

import APP.modele.MobileBullet;
import CORE.controleur.ASceneJeu;
import CORE.vaisseauxGraphiques.Vaisseau;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Classe abstraite représentant les ennemis
 */
public abstract class AEnnemi extends AMobile implements ITireur {

    protected static int NB_TIRS_MAXIMAL = 1;
    /**
     * Tous les ennemis partagent cet indicateur
     * pour qu'un nombre précis d'ennemi puisse tirer à la fois
     */
    protected static int nbTirs = 0;
    protected static int nbTirsDetruits = 0;

    //les constantes
    protected final int INTERVALLE_MIN = 200;
    protected final int INTERVALLE_MAX = 500;
    protected final int VITESSE_TIR = 7;

    //la scene
    protected ASceneJeu scene;

    //compteur du tir automatique
    protected int compteur = 1;
    protected int compteurIntervalle;

    //item stocké
    protected Vaisseau monItem;

    /**
     * Constructeur
     *
     * @param position
     * @param scene
     * @param vie
     */
    public AEnnemi(Position position, ASceneJeu scene, int vie) {
        super(position, 0, 0, vie);
        this.scene = scene;

        //Initialiser l'intervalle du tir
        Random rand = new Random();
        compteurIntervalle = rand.nextInt(INTERVALLE_MAX - INTERVALLE_MIN + 1) + INTERVALLE_MIN;
    }

    /**
     * Changer le nombre des tirs simultanés
     *
     * @param n
     */
    public static void setNbTirsSimultanes(int n) {
        NB_TIRS_MAXIMAL = n;
    }

    public static void resetTirs() {
        nbTirs = 0;
        nbTirsDetruits = 0;
    }

    /**
     * Ennemi peut toucher et etre touché par la balle (tirée par l'heros)
     *
     * @param sonMobile
     * @return
     */
    @Override
    public boolean peutToucher(IMobile sonMobile) {
        return sonMobile instanceof MobileBullet &&
                ((MobileBullet) sonMobile).getTireur() instanceof AHeros;
    }

    /**
     * Vérifier s'il est possible de tirer
     */
    protected void tirVerifier() {
        if (compteur++ == compteurIntervalle) {
            if (nbTirs < NB_TIRS_MAXIMAL) {
                nbTirs++;
                tirer();
            }
            compteur = 1;
        }
    }

    /**
     * Stocké un item dans cet ennemi. Il va jeter cet item avant son mort.
     *
     * @param item
     */
    public void ajouterItem(Vaisseau item) {
        this.monItem = item;
    }

    /**
     * Evenement de destruction
     */
    @Override
    public void detruire() {
        if (monItem != null)
            scene.ajouterVaisseau(monItem);
    }

    @Override
    public Position getPosition() {
        return position;
    }

    @Override
    public void deplacer() {
        deplacer(deltaX, deltaY);
        tirVerifier();
    }

    /**
     * LES METHODES IMPLEMENTEES DU ITIREUR
     */

    @Override
    public abstract void tirer();

    @Override
    public void debloquerTir() {
        if (++nbTirsDetruits == NB_TIRS_MAXIMAL)
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    nbTirs = 0;
                    nbTirsDetruits = 0;
                }
            }, 1000);
    }
}