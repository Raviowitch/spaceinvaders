package CORE.modele;

/**
 * Position
 */
public class Position {

    private int x;
    private int y;

    /**
     * Constructeur
     *
     * @param x
     * @param y
     */
    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Décaler horizontalement la position
     *
     * @param deltaX
     */
    public void shiftX(int deltaX) {
        this.x += deltaX;
    }

    /**
     * Décaler verticalement la position
     *
     * @param deltaY
     */
    public void shiftY(int deltaY) {
        this.y += deltaY;
    }

    /*
    LES GETTERS ET SETTERS
     */

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
