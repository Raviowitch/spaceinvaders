package CORE.modele;

import APP.modele.MobileBullet;
import APP.vue.SpriteHeros;
import CORE.controleur.ASceneJeu;
import CORE.controleur.Const;

/**
 * Classe abstraite représentant le héros
 */
public abstract class AHeros extends AMobile implements ITireur {
    protected final int VITESSE = 5; //vitesse par défaut

    //indicateur qui sert à bloquer le tir
    //pour que le heros ne puisse tirer qu'une balle à la fois
    protected boolean tirBloque = false;

    //la scène ou se trouve le heros
    protected ASceneJeu scene;

    /**
     * Constructeur
     *
     * @param position
     * @param scene
     * @param vie
     */
    public AHeros(Position position, ASceneJeu scene, int vie) {
        super(position, 0, 0, vie);
        this.position = position;
        this.scene = scene;
    }

    /**
     * Déplacer le mobile
     */
    @Override
    public void deplacer() {
        //capturer les touches
        controleTouche();

        //calculer la nouvelle position
        int newX = position.getX() + deltaX;
        int xLimite = Const.FENETRE_LARGEUR - SpriteHeros.LARGEUR_HEROS;

        //vérifier si la nouvelle position dépasse ou pas de la bordure
        if (newX < 0) {
            newX = 0;
        } else if (newX > xLimite) {
            newX = xLimite;
        }

        //affecter la nouvelle position
        position.setX(newX);
    }

    /**
     * Capturer les touches et changer la vitesse correspondante
     */
    protected abstract void controleTouche();

    /**
     * Tirer
     */
    @Override
    public abstract void tirer();

    /**
     * Indiquer si le heros peut tirer
     */
    @Override
    public void debloquerTir() {
        tirBloque = false;
    }

    /**
     * Peut toucher les balles tirées par les ennemis
     *
     * @param sonMobile
     * @return
     */
    @Override
    public boolean peutToucher(IMobile sonMobile) {
        return (sonMobile instanceof MobileBullet && ((MobileBullet) sonMobile).getTireur() instanceof AEnnemi)
                || sonMobile instanceof AItem;
    }
}
