package CORE.modele;

import APP.modele.MobileMur;
import CORE.vaisseauxGraphiques.Vaisseau;

/**
 * Classe abstraite représentant les balles
 */
public abstract class ABullet extends AMobile {
    protected ITireur tireur; //mon tireur
    protected int degat = 1; //dégat par défaut

    /**
     * Constructeur
     *
     * @param position
     * @param tireur
     * @param degat
     */
    public ABullet(Position position, ITireur tireur, int degat) {
        super(position, 0, 0, 1);

        this.position = position;
        this.tireur = tireur;
        this.degat = degat;
    }

    @Override
    public void estTouche(Vaisseau v) {
        etat = Etat.MORT;
    }

    @Override
    public boolean peutToucher(IMobile cible) {
        //ennemi vs heros
        return (tireur instanceof AEnnemi && cible instanceof AHeros)
                //heros vs ennemi
                || (tireur instanceof AHeros && cible instanceof AEnnemi)
                //bullet vs bullet
                || ((cible instanceof ABullet) && ((((ABullet) cible).getTireur() instanceof AHeros) == (tireur instanceof AEnnemi)))
                //bullet vs mur
                || (cible instanceof MobileMur);
    }

    public ITireur getTireur() {
        return this.tireur;
    }

    @Override
    public void detruire() {
        tireur.debloquerTir();
    }
}
