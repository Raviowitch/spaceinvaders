package CORE.modele;

import CORE.controleur.Const;
import CORE.vaisseauxGraphiques.Vaisseau;

public abstract class AItem extends AMobile {

    /**
     * Constructeur
     *
     * @param position
     */
    public AItem(Position position) {
        super(position, 0, 3, 1);
    }

    /**
     * Les items peuvent etre touchés par les héros
     *
     * @param sonMobile
     * @return
     */
    @Override
    public boolean peutToucher(IMobile sonMobile) {
        return sonMobile instanceof AHeros;
    }

    /**
     * L'item tombe jusqu'à ce qu'il touche la bordure de la fenetre
     */
    @Override
    public void deplacer() {
        if (position.getY() <= Const.FENETRE_HAUTEUR - 100)
            position.shiftY(deltaY);
    }

    @Override
    public void estTouche(Vaisseau v) {
        if (v.est("heros"))
            etat = Etat.MORT;
    }
}
