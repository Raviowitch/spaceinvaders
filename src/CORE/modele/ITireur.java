package CORE.modele;

/**
 * Interface représentant le tireur
 * Capacité fournie: tir
 */
public interface ITireur {

    /**
     * Tirer
     */
    public void tirer();

    /**
     * Débloquer la capacité de tir;
     * Cette méthode sert à libérer le tireur
     * au cas où le nombre de balles tirées
     * à la fois est limité
     */
    public void debloquerTir();
}