package CORE.modele;

import CORE.vaisseauxGraphiques.Vaisseau;

/**
 * Interface représentant les mobiles
 * Capacités fournies: déplacement, avoir une vie (peut etre touché et détruit)
 */
public interface IMobile {

    /**
     * Déplacer selon la vitesse
     * (deltaX et deltaY) prédéfinie
     */
    public void deplacer();

    /**
     * Déplacer avec la vitesse donnée
     *
     * @param deltaX
     * @param deltaY
     */
    public void deplacer(int deltaX, int deltaY);

    /**
     * Déplacer à une position précise
     *
     * @param newX
     * @param newY
     */
    public void deplacerA(int newX, int newY);

    /**
     * Inverser la direction de déplacement horizontal
     */
    public void changerDirectionH();

    /**
     * Inverser la direction de déplacement vertical
     */
    public void changerDirectionV();

    /**
     * Récupérer la position courante
     *
     * @return
     */
    public Position getPosition();

    /**
     * Evènement invoqué lors de la collision avec un autre mobile
     *
     * @param v
     */
    public void estTouche(Vaisseau v);

    /**
     * Evènement invoqué lors de la destruction du mobile
     */
    public void detruire();

    /**
     * Récupérer l'état courant du mobile
     *
     * @return
     */
    public Etat getEtat();

    /**
     * Récupérer la vie courante du mobile
     *
     * @return
     */
    public int getVie();
}
