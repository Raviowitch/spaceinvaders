package CORE.modele;

/**
 * Enum représentant l'état des mobiles
 */
public enum Etat {
    ENVIE,
    TOUCHE,
    MORT
}
