package CORE.modele;

import CORE.vaisseauxGraphiques.Vaisseau;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Classe la plus générale des mobiles qui implémente l'interface IMobile
 */
public abstract class AMobile implements IMobile {
    protected int deltaX, deltaY; //vitesse de déplacement
    protected Position position; //position
    protected int vie;//vie
    protected Etat etat;//état

    /**
     * Constructeur
     *
     * @param position
     * @param deltaX
     * @param deltaY
     * @param vie
     */
    public AMobile(Position position, int deltaX, int deltaY, int vie) {
        this.deltaX = deltaX;
        this.deltaY = deltaY;
        this.position = position;
        this.vie = vie;
        this.etat = Etat.ENVIE;
    }

    /**
     * Evènement invoqué lors de la destruction du mobile
     */
    @Override
    public void detruire() {
        //normalement cette méthode ne doit pas etre implémentée pour tous les mobiles
    }

    /**
     * Evènement invoqué lors de la collision avec un autre vaisseau (mobile)
     *
     * @param v Vaisseau heurté
     */
    @Override
    public void estTouche(Vaisseau v) {
        //Par défaut: traiter seulement la collision avec les balles
        if (v.getModele() instanceof ABullet) {
            //calculer la vie selon les dégats reçus
            vie -= ((ABullet) v.getModele()).degat;

            //changement d'état
            if (vie <= 0)
                etat = Etat.MORT;
            else {
                //si le mobile est toujours en vie:
                //changer son état pour Etat.TOUCHE pendant 100ms (pour l'affichage)
                etat = Etat.TOUCHE;
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        etat = vie > 0 ? Etat.ENVIE : Etat.MORT;
                    }
                }, 100);
            }
        }
    }

    /**
     * Déterminer la possibilité de la collision des mobiles selon leurs types
     * (afin d'économiser les calculs)
     *
     * @param sonMobile
     * @return
     */
    public abstract boolean peutToucher(IMobile sonMobile);

    /**
     * Récupérer la vie du mobile
     *
     * @return la vie du mobile
     */
    @Override
    public int getVie() {
        return vie;
    }

    /**
     * Récupérer l'état du mobile
     *
     * @return
     */
    @Override
    public Etat getEtat() {
        return etat;
    }

    /*
    METHODES POUR LE DEPLACEMENT DU MOBILE
     */

    @Override
    public void changerDirectionV() {
        deltaY *= -1;
    }

    @Override
    public void changerDirectionH() {
        deltaX *= -1;
    }

    @Override
    public Position getPosition() {
        return position;
    }

    @Override
    public void deplacer(int deltaX, int deltaY) {
        position.shiftX(deltaX);
        position.shiftY(deltaY);
    }

    @Override
    public void deplacerA(int newX, int newY) {
        position.setX(newX);
        position.setY(newY);
    }
}